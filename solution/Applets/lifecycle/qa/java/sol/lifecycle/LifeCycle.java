package qa.java.sol.lifecycle;

import java.applet.Applet;
import java.awt.*;

/*
 *
 * LifeCycle
 *
 */
public class LifeCycle extends Applet
{
	public LifeCycle()
	{
		//logMessage("Constructor");
	}

	public void init()
	{
		logMessage("init()");

	}

	public void start()
	{
		logMessage("start()");
	}

	public void paint(Graphics g)
	{
		logMessage("paint()");
		g.drawString("My string", 1, 10);
	}

	public void stop()
	{
		logMessage("stop()");
	}

	public void destroy()
	{
		logMessage("destroy()");
	}

    // allows us to change where we output the messages
    // without having to change all the code above
	void logMessage(String s)
	{
		// This is used for the first part of the
		// question:
		//
	      showStatus("LifeCycle: " + s);

		// And this for the second.
		//
		// Note that the "\r" is required due to
		// Notepad not understanding just a
		// linefeed.
		// System.out.println("LifeCycle: " + s + "\r");
	}

}