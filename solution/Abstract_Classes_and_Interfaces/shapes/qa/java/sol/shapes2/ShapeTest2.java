package qa.java.sol.shapes2;

import java.applet.*;
import java.awt.*;

/*
 *
 * ShapeTest2
 *
 */

//
// Alternative solution using Drawable interface.
// Note that there is no reference to the abstract
// class MyShape2.  ShapeTest2 can draw an object
// of any class that implements Drawable interface.
//

public class ShapeTest2 extends Applet
{
	
		// Create a polymorphic array of
		// any four Drawable objects. In this case,
		// two are MyRectangle2 objects and two are
		// are MyCircle2 objects.
		//	

		Drawable[] myShapes = {
			new MyRectangle2(10,10,200,200),	// left, top, width, height
			new MyCircle2(110,110,100),			// centreX, centreY, radius
			new MyRectangle2(200,30,100,50),
			new MyCircle2(250,80,50)
					  };

		


	public void paint(Graphics g)
	{

		//
		// Ask each shape in the array to draw itself.
		// Note that the draw() method of the Drawable interface
		// class takes a single argument: a reference
		// to a Graphics object, i.e. as passed into paint().
		//
		for (int i=0; i < myShapes.length; i++)
		{
			myShapes[i].draw(g);
		}
	}
}
