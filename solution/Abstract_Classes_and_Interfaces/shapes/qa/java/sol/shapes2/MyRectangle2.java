package qa.java.sol.shapes2;

import java.awt.*;

/*
 *
 * MyRectangle2
 *
 */

public class MyRectangle2 extends MyShape2 implements Drawable
{
	//
	// Constructor
	//
	public MyRectangle2(int l, int t, int w, int h)
	{
		super(l, t, w, h);
	}


	//
	// Must implement draw method defined in Drawable interface
	//
	public void draw(Graphics g)
	{
		//
		// Draw the rectangle
		//
		g.drawRect(left,top,width,height);
	}
}
