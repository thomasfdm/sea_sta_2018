package qa.java.sol.hibernate2;
import javax.persistence.*;

@Entity
@DiscriminatorValue("DA")
public class DepositAccount extends BankAccount implements java.io.Serializable {

	@Column(name="max_number_trans")
	private int maxDailyTransactions;

	public int getMaxDailyTransactions() {
		return maxDailyTransactions;
	}

	public void setMaxDailyTransactions(int maxDailyTransactions) {
		this.maxDailyTransactions = maxDailyTransactions;
	}



}
