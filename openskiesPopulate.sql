-- clear the bank_transactions table first (because of referential integrity)
delete from bank_transactions;
-- then clear the bank_accounts table
delete from bank_accounts;

-- load bank_accounts table
insert into bank_accounts 
  values ( 9999995,'Adam Axminster',	 6500.00, 5000.00, NULL);

insert into bank_accounts 
  values ( 9999996,'Belinda Berylium', 7700.00, NULL, 3);


-- load bank_transactions table
insert into bank_transactions 
  values ( 1,'2014-10-31',	1234.11, 'C', 9999995);
insert into bank_transactions 
  values ( 2,'2014-11-05',	2100.00, 'C', 9999995);
insert into bank_transactions 
  values ( 3,'2014-12-25',	3401.12, 'C', 9999995);
insert into bank_transactions 
  values ( 4,'2014-12-31',	 234.23, 'D', 9999995);
  
insert into bank_transactions 
  values ( 5,'2014-05-01',	7700.00, 'C', 9999996);
 