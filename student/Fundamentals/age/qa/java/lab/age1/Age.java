/* Using variables, assignments and operators */

package qa.java.lab.age1;

public class Age
{
	public static void main(String[] args)
	{

		String s = "hello";
		meth(s);
		System.out.println(s);

		int myAge = 21, partnerAge = 22;
		int diff = Math.abs(myAge - partnerAge);

		System.out.println("My age is " + myAge);

		System.out.println("Difference: " + diff);
		System.out.println((myAge + partnerAge) / 2.0);


		System.out.println("End of program");


	}

	public static void meth(String x){
		x = x + "World";
	}

}






